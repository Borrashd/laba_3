#include "functions.h"

QString Caesar(QString str, int k)
{
    QString AA = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
    QString aa = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    int *c = new int[str.length()];
    for (int i = 0; i < str.length(); i++)
    {
        for (int j = 0; j < 33; j++)
        {
            if (str[i] == AA[j])
            {
                c[i] = j;
            }
            else if (str[i] == aa[j])
            {
                c[i] = j;
            }
        }
    }
    for (int i = 0; i < str.length(); i++)
    {
        for (int j = 0; j < 33; j++)
        {
            if ((c[i] + k) % 33 == AA[j])
            {
                str[i] = AA[i];
            }
            else if ((c[i] + k) % 33 == aa[j])
            {
                str[i] = aa[i];
            }
        }
    }
    return str;
}
